package pathfinding

type experiment struct {
	Rows    int
	Columns int

	Source [2]int // The start, defined by a ^ character
	Sink   [2]int // The end, defined by a $ character

	generation uint64
	solutions  []Solution
}

func (e *experiment) Continue() error {
	panic("implement me")
}

func (e *experiment) Genesis(populationSize uint64) error {
	panic("implement me")
}

func (e *experiment) Crossover() error {
	panic("implement me")
}

func (e *experiment) Mutate() error {
	panic("implement me")
}

func (e *experiment) Selection() error {
	panic("implement me")
}

func (e *experiment) Utility() float64 {
	panic("implement me")
}

func (e *experiment) Top(n uint64) []interface{} {
	panic("implement me")
}
