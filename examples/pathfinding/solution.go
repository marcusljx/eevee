package pathfinding

import "math"

// Solution refers to a possible sequence of coordinates
type Solution [][2]int

// HeadHammingDistanceTo returns the hamming distance of the
// first coordinate of the sequence to the given target
func (s Solution) HeadHammingDistanceTo(target [2]int) int {
	if len(s) == 0 {
		return math.MaxInt32
	}
	return hamming(s[0], target)
}

// TailHammingDistanceTo returns the hamming distance of the
// last coordinate of the sequence to the given target
func (s Solution) TailHammingDistanceTo(target [2]int) int {
	if len(s) == 0 {
		return math.MaxInt32
	}
	return hamming(s[len(s)-1], target)
}

// IsConsecutive returns true if the sequence is consecutive,
// ie. each element is adjacent to its neighbouring elements.
// Identical coordinates and diagonally adjacent coordinates
// are not considered consecutive.
func (s Solution) IsConsecutive() bool {
	if len(s) <= 1 {
		return true
	}

	for i, b := range s[1:] {
		if !consecutive(s[i], b) {
			return false
		}
	}
	return true
}

// Len returns the length of the solution
func (s Solution) Len() int {
	return len(s)
}

func diff(a, b int) int {
	if a > b {
		return a - b
	}
	return b - a
}

func hamming(a, b [2]int) int {
	return diff(a[0], b[0]) + diff(a[1], b[1])
}

func consecutive(a, b [2]int) bool {
	if a == b {
		return false
	}
	if diff(a[0], b[0]) > 1 || diff(a[1], b[1]) > 1 {
		return false
	}

	if (a[0] == b[0]) != (a[1] == b[1]) {
		return true
	}
	return false
}
