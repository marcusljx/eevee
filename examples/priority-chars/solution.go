package main

// NewSolution returns a new Solution
func NewSolution(size int, chosenOne byte) *Solution {
	return &Solution{
		data:       generateRandomByteSlice(size),
		chosenByte: chosenOne,
		score:      -1,
	}
}

// Solution is a potential data solution
type Solution struct {
	data       []byte
	chosenByte byte
	score      float64 // caches the earlier score
}

// Score returns the score of the solution
func (s *Solution) Score() (result float64) {
	if s.score != -1 {
		return s.score
	}

	for _, c := range s.data {
		if c == s.chosenByte {
			result++
		}
	}

	return
}

func dist(a, b byte) byte {
	if a > b {
		return a - b
	}
	return b - a
}

// ResetScore forces Score() to recalculate the score the next time Score() is called
func (s *Solution) ResetScore() {
	s.score = -1
}

// String implements fmt.Stringer
func (s *Solution) String() string {
	return string(s.data)
}

func generateRandomByteSlice(n int) []byte {
	result := make([]byte, n)
	for i := range result {
		result[i] = byte('A' + rng.Intn(26))
	}
	return result
}
