package main

// Selection simply sorts the solutions in descending Score() order,
// and trims the population to the first N Solutions
func (h *HighstringExperiment) Selection() error {
	h.population = h.top(h.populationSize)
	//log.Printf("size = %d", len(h.population))
	return nil
}
