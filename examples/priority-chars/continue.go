package main

import "fmt"

// Continue returns a non-nil error iff:
//  - all solutions in the population are exactly the same
//  - utility is > 0
func (h *HighstringExperiment) Continue() error {
	fmt.Printf("\t%+v", h.population)

	//for _, s := range h.population[1:] {
	//	if string(s.data) != string(h.population[0].data) {
	//		return nil
	//	}
	//}
	//return fmt.Errorf("all solutions are exactly the same")

	return nil
}
