package main

// Genesis creates the initial start population.
func (h *HighstringExperiment) Genesis(populationSize uint64) error {
	h.populationSize = populationSize
	h.population = make([]*Solution, populationSize)
	for i := range h.population {
		h.population[i] = NewSolution(h.StringLength, h.ChosenByte)
	}
	return nil
}
