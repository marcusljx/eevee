package main

// Utility returns the highest score in the population
func (h *HighstringExperiment) Utility() (score float64) {
	for _, s := range h.population {
		if newScore := s.Score(); newScore > score {
			score = newScore
		}
	}
	return
}
