package main

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSwapByHalf(t *testing.T) {
	harness := func(a, b, expectedA, expectedB string) {
		t.Run(a+","+b, func(t2 *testing.T) {
			actual1, actual2 := swapHeadTail([]byte(a), []byte(b), len(a)/2)

			if string(actual1) != expectedA || string(actual2) != expectedB {
				t2.Errorf("expectedA:\n%v", cmp.Diff(expectedA, string(actual1)))
				t2.Errorf("expectedB:\n%v", cmp.Diff(expectedB, string(actual2)))
			}
		})
	}

	harness("", "", "", "")
	harness("a", "b", "b", "a")
	harness("aa", "bb", "ba", "ba")
	harness("abc", "xyz", "zbc", "xya")
	harness("aabb", "xxyy", "yybb", "xxaa")
}
