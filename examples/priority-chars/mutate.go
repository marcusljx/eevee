package main

// Mutate iterates performs a random mutation on a cloned 50% of the population,
// and adds them back into the population.
// The mutation involves randomly selecting n/2 bytes and mutating them
func (h *HighstringExperiment) Mutate() error {
	mutated := make([]*Solution, 0, len(h.population)/2)

	for _, solution := range h.population {
		if rng.Float64() < 0.5 && len(mutated) < len(h.population)/2 {
			mutated = append(mutated, mutate(solution))
		}
	}

	h.population = append(h.population, mutated...)
	return nil
}

func mutate(s *Solution) *Solution {
	return &Solution{
		data:       mutateBytes(s.data, 0.2),
		chosenByte: s.chosenByte,
		score:      -1,
	}
}

func mutateBytes(data []byte, prob float64) []byte {
	result := make([]byte, len(data))
	copy(result, data)

	for i := range result {
		if rng.Float64() < prob {
			result[i] = randomAlphabet()
		}
	}
	return result
}

func randomAlphabet() byte {
	return 'A' + byte(rng.Intn(26))
}
