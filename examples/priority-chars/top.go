package main

import "sort"

// Top returns the top n results in the current population.
func (h *HighstringExperiment) Top(n uint64) interface{} {
	return h.top(n)
}

func (h *HighstringExperiment) top(n uint64) []*Solution {
	sort.Slice(h.population, func(i, j int) bool {
		return h.population[i].Score() > h.population[j].Score()
	})

	return h.population[:n]
}
