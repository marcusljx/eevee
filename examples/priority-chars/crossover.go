package main

import "fmt"

// Crossover performs a pairwise pairing (ie. {0,1}, {2,3}...),
// and runs swapHeadTail on each pair.
// The resulting pairs are appended to the population.
func (h *HighstringExperiment) Crossover() error {
	newSolutions := make([]*Solution, 0, len(h.population))

	for i := 0; i < len(h.population)/2; i++ {
		a := h.population[i]
		b := h.population[i+(len(h.population)/2)]

		newData1, newData2 := swapHeadTail(a.data, b.data, len(a.data)*3/4)

		newSolutions = append(newSolutions,
			&Solution{
				data:       newData1,
				chosenByte: h.ChosenByte,
				score:      -1,
			},
			&Solution{
				data:       newData2,
				chosenByte: h.ChosenByte,
				score:      -1,
			},
		)
	}

	h.population = append(h.population, newSolutions...)
	return nil
}

// swapHeadTail returns a half-half crossover between a and b.
// if both []byte consists of a single character, they are swapped.
// If len(a) != len(b), this function panics.
// If len(a) or len(b) is odd, the split occurs before the middle element.
func swapHeadTail(a, b []byte, size int) ([]byte, []byte) {
	n := len(a)
	if n != len(b) {
		panic(fmt.Sprintf("swapHeadTail called on []byte inputs of different size : len(a) = %d, len(b) = %d", len(a), len(b)))
	}
	if n == 0 {
		return []byte{}, []byte{}
	}

	new1, new2 := make([]byte, n), make([]byte, n)
	copy(new1, a)
	copy(new2, b)

	if n == 1 {
		return new2, new1 // just swap wholesale
	}

	copy(new1, b[n-size:])
	copy(new2[n-size:], a)

	return new1, new2
}
