package main

import (
	"math/rand"
	"time"
)

var (
	// Experiment is the default experiment
	Experiment = HighstringExperiment{
		ChosenByte:   'Z',
		StringLength: 20,
	}

	rng = rand.New(rand.NewSource(time.Now().UnixNano()))
)

// HighstringExperiment is an experiment which generates the best string of chars that best fulfill the following:
//
//   - each right-adjacent char is larger than its left neighbour
//   - the rightmost char is as close (value difference) to the ChosenByte as possible (following ASCII values)
type HighstringExperiment struct {
	ChosenByte     byte
	StringLength   int
	population     []*Solution
	populationSize uint64
}
