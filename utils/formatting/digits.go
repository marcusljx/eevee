package formatting

// DecimalDigits returns the number of digits (in decimal form) of a uint64
func DecimalDigits(x uint64) int {
	var (
		digits = 1
		c      = 10.0
		f      = float64(x)
	)
	for f >= c {
		c *= 10
		digits++
	}
	return digits
}
