package formatting

import (
	"fmt"
	"testing"
	"testing/quick"
)

func decimalDigitsAlternative(x uint64) int {
	return len(fmt.Sprint(uint64(x)))
}

func TestDecimalDigits(t *testing.T) {
	if err := quick.CheckEqual(DecimalDigits, decimalDigitsAlternative, nil); err != nil {
		t.Errorf("quick.CheckEqual : %v", err)
	}
}

func TestDecimalDigits2(t *testing.T) {
	var c uint64 = 10
	for i := 0; i < 64; i++ {
		expected := decimalDigitsAlternative(c)
		actual := DecimalDigits(c)
		if actual != expected {
			t.Errorf("DecimalDigits(%d) = %d; want %d", c, actual, expected)
		}

		c *= 10
	}
}

func BenchmarkDecimalDigits(b *testing.B) {
	for i := 0; i < b.N; i++ {
		DecimalDigits(uint64(i))
	}
}

func BenchmarkDecimalDigits_UsingSprintLen(b *testing.B) {
	for i := 0; i < b.N; i++ {
		decimalDigitsAlternative(uint64(i))
	}
}
