all: eevee example-priority-chars

example-priority-chars: eevee priority-chars
	./bin/eevee -m=1 --max-score=20 -p ./bin/priority-chars.so

.PHONY: eevee priority-chars

priority-chars:
	go build -o=bin/priority-chars.so -buildmode=plugin ./examples/priority-chars

eevee:
	go build -o=bin/eevee .