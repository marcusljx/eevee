package main

// NegativeProbabilityError is the surfaced error type when negative probability is encountered
type NegativeProbabilityError string

// Error implements builtin error
func (n NegativeProbabilityError) Error() string {
	return "unexpected negative probability : " + string(n)
}

// Over1ProbabilityError is the surfaced error type when an over-1 probability is encountered
type Over1ProbabilityError string

// Error implements builtin error
func (o Over1ProbabilityError) Error() string {
	return "unexpected probability above 1.0 : " + string(o)
}

// UnrecognisedSymbolError is the surfaced error type when an unrecognised symbol is encountered
type UnrecognisedSymbolError string

// Error implements builtin error
func (u UnrecognisedSymbolError) Error() string {
	return "unrecognised symbol " + string(u)
}
