package eevee

// Experiment defines the runnable interface for all eevee experiment plugins
type Experiment interface {
	// Genesis creates the initial start population.
	Genesis(populationSize uint64) error

	// Crossover performs the crossover operations.
	// How this is done across the population in terms
	// of pair-wise selection is implementation specific.
	Crossover() error

	// Mutate performs the crossover operations
	// How this is done across the population in terms
	// of selection is implementation specific.
	Mutate() error

	// Selection performs the internal selection process for the next generation
	Selection() error

	// Utility returns the "normalised" score value across the current population
	Utility() float64

	// Top returns the top n results in the current population.
	// The returned interface should be iterable.
	Top(n uint64) interface{}

	// Continue returns a non-nil error if the experiment has reached an end
	// eg. all solutions are are already optimal
	// The error message is printed but will not be surfaced as
	// a runner error
	Continue() error
}
