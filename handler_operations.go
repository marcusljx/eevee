package main

import (
	"fmt"
	"plugin"

	"gitlab.com/marcusljx/eevee/public/eevee"
)

func (h *Handler) loadExperiment(args []string) error {
	var (
		file   = args[0] // checked by cobra(min=1)
		symbol = "Experiment"
	)
	if len(args) > 1 {
		symbol = args[1] // checked by cobra(max=2)
	}

	p, err := plugin.Open(file)
	if err != nil {
		return fmt.Errorf("plugin.Open : %v", err)
	}

	s, err1 := p.Lookup(symbol)
	if err1 != nil {
		return fmt.Errorf("%T.Lookup : %v", p, err1)
	}

	if experiment, ok := s.(eevee.Experiment); ok {
		h.experiment = experiment
		return nil
	}
	return UnrecognisedSymbolError(fmt.Sprintf("%T.(%s)", s, symbol))
}

func (h *Handler) iterate() error {
	if h.rng.Float64() < h.CrossoverProbability {
		if err := h.experiment.Crossover(); err != nil {
			return fmt.Errorf("crossover : %v", err)
		}
	}
	if h.rng.Float64() < h.MutationProbability {
		if err := h.experiment.Mutate(); err != nil {
			return fmt.Errorf("mutate : %v", err)
		}
		//fmt.Printf("\tmutated")
	}

	if err := h.experiment.Selection(); err != nil {
		return fmt.Errorf("selection : %v", err)
	}

	return nil
}
