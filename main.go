package main

import (
	"fmt"
	"math/rand"
	"os"
	"time"

	"github.com/spf13/cobra"
)

var (
	handler = &Handler{
		rng:                  rand.New(rand.NewSource(time.Now().UnixNano())),
		InitialPopulation:    10,
		MaxGenerations:       100000,
		MaxScore:             -1,
		CrossoverProbability: 1.0,
		MutationProbability:  0.1,
		MaxShow:              10,
	}

	rootCmd = &cobra.Command{
		Use:     "eevee <FILE> [EXPERIMENT=Experiment]",
		Example: "eevee optimise-abc.so WithoutParameter513",
		Short:   "eevee is a commandline-based executor for generational experiments",
		Long:    "eevee is a commandline-based executor for generational experiments",

		SilenceUsage:  true,
		SilenceErrors: true,

		Args: func(cmd *cobra.Command, args []string) error {
			if err := cobra.MinimumNArgs(1)(cmd, args); err != nil {
				return err
			}
			if err := cobra.MaximumNArgs(2)(cmd, args); err != nil {
				return err
			}
			return nil
		},

		PreRunE:  handler.PreRunE,
		RunE:     handler.RunE,
		PostRunE: handler.PostRunE,
	}
)

func main() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	rootCmd.Flags().Uint64VarP(&handler.InitialPopulation, "population-size", "s", handler.InitialPopulation, "population size of each generation")
	rootCmd.Flags().Uint64Var(&handler.MaxGenerations, "max-generations", handler.MaxGenerations, "maximum number of generations to iterate over")
	rootCmd.Flags().Float64Var(&handler.MaxScore, "max-score", handler.MaxScore, "maximum score for the experiment. This is used to terminate the experiment early. Leave as -1 if highest possible is desired")

	rootCmd.Flags().Float64VarP(&handler.CrossoverProbability, "crossover-probability", "c", handler.CrossoverProbability, "probability to run crossover for each solution set within each generation")
	rootCmd.Flags().Float64VarP(&handler.MutationProbability, "mutation-probability", "m", handler.MutationProbability, "probability to run mutation for each solution set within each generation")

	rootCmd.Flags().Uint64Var(&handler.MaxShow, "max-show", handler.MaxShow, "number of top results to show after the experiment is terminated.")
	rootCmd.Flags().BoolVarP(&handler.PromptStart, "prompt-start", "p", handler.PromptStart, "if set, shows the initial population and prompts the user to start the iteration process")
}
