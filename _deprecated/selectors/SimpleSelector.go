package selectors

import (
	"sort"

	interfaces2 "gitlab.com/marcusljx/eevee/_deprecated/interfaces"
)

// SimpleSelector is a basic selector.
// It selects the best r entities from a pool of N entities, where r is a ratio (float between 0 and 1)
type SimpleSelector struct {
	selectionRatio float64
}

// NewSimpleSelection returns a new SimpleSelector object
func NewSimpleSelection(ratio float64) *SimpleSelector {
	return &SimpleSelector{
		selectionRatio: ratio,
	}
}

// Do performs the selection operation
func (s *SimpleSelector) Do(entities ...interfaces2.SolutionEntity) []interfaces2.SolutionEntity {
	e := interfaces2.EntityArray(entities)
	sort.Sort(e)

	trimmedPopulationSize := int(float64(len(e)) * s.selectionRatio)
	return []interfaces2.SolutionEntity(e[len(e)-trimmedPopulationSize:])
}
