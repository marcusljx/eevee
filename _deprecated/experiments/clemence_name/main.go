package main

import (
	"log"
	"os"

	crossovers2 "gitlab.com/marcusljx/eevee/_deprecated/crossovers"
	entities2 "gitlab.com/marcusljx/eevee/_deprecated/entities"
	experiments2 "gitlab.com/marcusljx/eevee/_deprecated/experiments"

	"gitlab.com/marcusljx/eevee/_deprecated/interfaces"
	"gitlab.com/marcusljx/eevee/_deprecated/mutations"
	"gitlab.com/marcusljx/eevee/_deprecated/rand"
	"gitlab.com/marcusljx/eevee/_deprecated/selectors"
)

var (
	upperAlphabet = []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
	entityLength  = 8
)

type ClemenceNameExperiment struct {
	interfaces.BaseExperiment
}

func initialisePopulation(n int) (result []interfaces.SolutionEntity) {
	result = make([]interfaces.SolutionEntity, n)
	for i := 0; i < n; i++ {
		result[i] = entities2.NewClemenceNameEntity(rand.String(entityLength, upperAlphabet...))
	}
	return
}

func main() {
	x := new(ClemenceNameExperiment)
	x.Crossover = crossovers2.NewSimpleCrossover(0.5)
	x.Mutation = mutations.NewShiftMutation(0.2, upperAlphabet...)
	x.Selector = selectors.NewSimpleSelection(0.5)
	x.CycleLimit = 1000
	x.ScoreThreshold = 8
	x.InitialPopulation = initialisePopulation(20)

	err := experiments2.Run(x)
	if err != nil {
		log.Printf("experiment returned error: %v", err)
		os.Exit(1)
	}
}
