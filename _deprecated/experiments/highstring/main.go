package main

import (
	"flag"

	crossovers2 "gitlab.com/marcusljx/eevee/_deprecated/crossovers"
	entities2 "gitlab.com/marcusljx/eevee/_deprecated/entities"
	experiments2 "gitlab.com/marcusljx/eevee/_deprecated/experiments"

	"log"
	"os"

	"gitlab.com/marcusljx/eevee/_deprecated/interfaces"
	"gitlab.com/marcusljx/eevee/_deprecated/mutations"
	"gitlab.com/marcusljx/eevee/_deprecated/rand"
	"gitlab.com/marcusljx/eevee/_deprecated/selectors"
)

var (
	upperAlphabet = []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
	entityLength  = flag.Int("entity-length", 10, "Specify the length of the solution entity.")
)

// HighStringExperiment demonstrates a simple experiment that aims to discover a string of the following properties:
//  The closer a character is to "Z", the higher its score is. "Z" is the "best" character.
//  A string (SolutionEntity) has no knowledge of itself.
//  Each SolutionEntity encounters operations that changes its structure.
//  Each generation's population is made up of the top SolutionEntities from the previous generation,
//  and the "new" 10 that are modified clones of the top half.
//  Of these, the top half of the total population will move on to the next generation
type HighStringExperiment struct {
	interfaces.BaseExperiment
}

func initialisePopulation(n int) (result []interfaces.SolutionEntity) {
	result = make([]interfaces.SolutionEntity, n)
	for i := 0; i < n; i++ {
		result[i] = entities2.NewSimpleTextEntity(rand.String(*entityLength, upperAlphabet...))
	}
	return
}

func main() {
	x := new(HighStringExperiment)
	x.Crossover = crossovers2.NewSimpleCrossover(0.5)
	x.Mutation = mutations.NewShiftMutation(0.2, upperAlphabet...)
	x.Selector = selectors.NewSimpleSelection(0.5)
	x.CycleLimit = 400
	x.ScoreThreshold = 900
	x.InitialPopulation = initialisePopulation(20)

	err := experiments2.Run(x)
	if err != nil {
		log.Printf("experiment returned error: %v", err)
		os.Exit(1)
	}
}
