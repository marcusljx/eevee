package mutations

import (
	"math/rand"
	"testing"
	"time"

	entities2 "gitlab.com/marcusljx/eevee/_deprecated/entities"

	"fmt"

	"github.com/stretchr/testify/assert"
)

var (
	UpperAlphabet = []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
)

func TestShiftMutation_Do(t *testing.T) {
	rand.Seed(time.Now().Unix())
	b := entities2.NewSimpleTextEntity(msg)

	c := NewShiftMutation(1.0, UpperAlphabet...)
	err := c.Do(b)
	assert.NoError(t, err)
	assert.NotEqual(t, msg, fmt.Sprint(b))
}
