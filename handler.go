package main

import (
	"fmt"
	"math/rand"
	"reflect"

	"github.com/spf13/cobra"

	"gitlab.com/marcusljx/eevee/public/eevee"
	"gitlab.com/marcusljx/eevee/utils/formatting"
)

// Handler is the root handler
type Handler struct {
	experiment eevee.Experiment
	rng        *rand.Rand

	InitialPopulation uint64
	MaxGenerations    uint64
	MaxScore          float64

	CrossoverProbability float64 // domain: [0,1]
	MutationProbability  float64 // domain: [0,1]

	MaxShow     uint64
	PromptStart bool
}

// PreRunE implements the cobra.Command execution logic
func (h *Handler) PreRunE(_ *cobra.Command, args []string) error {
	if h.CrossoverProbability < 0 {
		return NegativeProbabilityError("--crossover-probability")
	}
	if h.CrossoverProbability > 1 {
		return Over1ProbabilityError("--crossover-probability")
	}
	if h.MutationProbability < 0 {
		return NegativeProbabilityError("--mutation-probability")
	}
	if h.MutationProbability > 1 {
		return Over1ProbabilityError("--mutation-probability")
	}

	if err := h.loadExperiment(args); err != nil {
		return fmt.Errorf("loadExperiment : %v", err)
	}

	return nil
}

// RunE implements the cobra.Command execution logic
func (h *Handler) RunE(_ *cobra.Command, _ []string) error {
	if err := h.experiment.Genesis(h.InitialPopulation); err != nil {
		return fmt.Errorf("error initialising population : %v", err)
	}

	var (
		gen       uint64
		genFormat = "G-" + fmt.Sprintf("%%%dd", formatting.DecimalDigits(h.MaxGenerations))
	)

	// initial
	fmt.Printf(genFormat, gen)
	fmt.Printf("\tUtility: %f", h.experiment.Utility())
	if reason := h.experiment.Continue(); reason != nil {
		fmt.Printf("\tContinue() : %v\n", reason)
		return nil
	}
	fmt.Println()
	if h.PromptStart {
		fmt.Println("Press Enter to Begin:")
		if _, err := fmt.Scanln(); err != nil {
			return fmt.Errorf("internal error while scanning for user input : fmt.Scanln : %v", err)
		}
	}

	// iterations
	for gen = 1; gen <= h.MaxGenerations; gen++ {
		fmt.Printf(genFormat, gen)
		if err := h.iterate(); err != nil {
			return fmt.Errorf("\titeration error : %v", err)
		}

		fmt.Printf("\tUtility: %f", h.experiment.Utility())

		if h.MaxScore != -1 && h.experiment.Utility() >= h.MaxScore {
			fmt.Printf("\tmax-score reached : %f >= %f\n", h.experiment.Utility(), h.MaxScore)
			return nil
		}

		if reason := h.experiment.Continue(); reason != nil {
			fmt.Printf("\tContinue() : %v\n", reason)
			return nil
		}

		fmt.Println()
	}
	fmt.Printf("max-generations reached : %d iterations\n", h.MaxGenerations)

	return nil
}

// PostRunE implements the cobra.Command execution logic
func (h *Handler) PostRunE(_ *cobra.Command, _ []string) error {
	t := h.experiment.Top(h.MaxShow)

	fmt.Println("--- Final Population ---")
	sliceValue := reflect.ValueOf(t)
	if sliceValue.Len() < int(h.MaxShow) {
		return fmt.Errorf("retrieved Top() had size %d:\n%+v", sliceValue.Len(), t)
	}
	for i := 0; i < sliceValue.Len(); i++ {
		fmt.Printf("# %d : %+v\n", i, sliceValue.Index(i).Interface())
	}
	return nil
}
